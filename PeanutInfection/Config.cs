﻿using GameCore;

namespace PeanutInfection
{
    public static class Config
    {

        public static bool EnableByDefault;
        public static bool EnableRespawn;
        public static int InfectedHP;

        public static void ReloadConfig()
        {
            EnableByDefault = ConfigFile.ServerConfig.GetBool("pi_enablebydefault", true);
            EnableRespawn = ConfigFile.ServerConfig.GetBool("pi_enablerespawn", false);
            InfectedHP = ConfigFile.ServerConfig.GetInt("pi_infectedhp", 800);
        }
    }
}
